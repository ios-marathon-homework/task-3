import UIKit

// #1

func funcClouser (funtion : () -> ()){

    for i in 0..<10{
        print(i)
    }
    funtion()
}

funcClouser(){
    print("finish")
}

//#3

let arrayOfInt : [Int] = [1, 3, 4, 9, 2, 5, 7, 8, 10, 6]

func search(array: [Int], funtion: (Int, Int?)->Bool) -> Int{
    var optionalVariable : Int? = nil
    for i in array {
        if funtion(i, optionalVariable){
            optionalVariable = i
        }
    }
    return optionalVariable!
}

search(array: arrayOfInt , funtion: {
    $1 == nil || $0 < $1!
})

search(array: arrayOfInt , funtion: {
    $1 == nil || $0 > $1!
})


//#4

func priority(str : String) -> Int{
    switch(str){
    case "a", "f", "g", "o", "m", "y": return 0
    case "A", "F", "G", "O", "M", "Y": return 1
    case "a"..."z": return 2
    case "A"..."Z": return 3
    case "0"..."9": return 4
    default: return 5
    }
}

let a = "1"
let b = "."

switch (priority(str: a), priority(str: b)){
case let(x,y) where x < y: print(a)
case let(x,y) where x > y: print(b)
default: print(a <= b ? a : b)
}


let str = "sdfjsldkjflsdjJGOSJDOGIJojoi4545"

var strCharacter = [String]()

for i in str{
    strCharacter.append(String(i))
}



let sorted = strCharacter.sorted{
    switch (priority(str: $0), priority(str: $1)){
    case let(x,y) where x < y: return true
    case let(x,y) where x > y: return false
    default: return $0 <= $1
    }
}

print(sorted)

//#5

var randomStr = "KDMFkmldf;dfgdfgdfgrep"
var randomStrCharacter = [String]()

for i in randomStr{
    randomStrCharacter.append(String(i))
}

func searchOptimisedCharacter(array: [String], funtion: (String, String)->Bool) -> String{
    if array.count != 0 {
        var variable = array[0]
        for i in array {
            if funtion(i, variable){
                variable = i
            }
        }
        return String(variable)
    }
    else{
        return String("Используйте другой массив")
    }
}

print(
    searchOptimisedCharacter(array: randomStrCharacter, funtion: {
        return $0 > $1
    })
)

print(
    searchOptimisedCharacter(array: randomStrCharacter, funtion: {
        return $0 < $1
    })
)

